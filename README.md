# Juego Gato en C

Juego del gato [México] o 3 en raya, un proyecto simple desarrollado en C.

# Requerimientos 
- GCC
- GLIBS
- C LIBS

## Compilar
```
gcc -o gatogame gato.c
```

## Ejecutar

```
./gatogame
```

## Capturas

![alt tag](1.gif)



## MIT License

License: MIT

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

#### Developed By
----------------
 * linuxitos - <contact@linuxitos.com>