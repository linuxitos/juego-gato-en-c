#include <stdio.h>
#include <stdlib.h>
void llenar(char c[3][3]){
	char l;
	l='1';
	int i,j;
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			c[i][j]=l++;
		}
	}
}
void tablero(char c[3][3]){
	int i,j;
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			if(j<2){
				printf(" %c |",c[i][j]);
			}else{
				printf(" %c ",c[i][j]);
			}
		}
		if(i<2){
			printf("\n-----------\n");
		}
	}
	printf("\n\n");
}
void introducir(char c[3][3],char b){
	int i,j,k;
	char aux;
	printf("Jugador %c ingrese casilla: ", b);
	do{
		do{
			
			fflush(stdin);
			scanf("%c",&aux);
		}while(aux<'1' || aux>'9');
		k=0;
		switch(aux){
			case '1':{
				i=0;
				j=0;
				if(c[i][j]=='X' ||c[i][j]=='O'){
					k=1;
					printf("Casilla cupada\n");
				}
				break;
			}
			case '2':{
				i=0;
				j=1;
				if(c[i][j]=='X' ||c[i][j]=='O'){
					k=1;
					printf("Casilla ocupada\n");
				}
				break;
			}
			case '3':{
				i=0;
				j=2;
				if(c[i][j]=='X' ||c[i][j]=='O'){
					k=1;
					printf("Casilla ocupada\n");
				}
				break;
			}
			case '4':{
				i=1;
				j=0;
				if(c[i][j]=='X' ||c[i][j]=='O'){
					k=1;
					printf("Casilla ocupada\n");
				}
				break;
			}
			case '5':{
				i=1;
				j=1;
				if(c[i][j]=='X' ||c[i][j]=='O'){
					k=1;
					printf("Casilla ocupada\n");
				}
				break;
			}
			case '6':{
				i=1;
				j=2;
				if(c[i][j]=='X' ||c[i][j]=='O'){
					k=1;
					printf("Casilla ocupada\n");
				}
				break;
			}
			case '7':{
				i=2;
				j=0;
				if(c[i][j]=='X' ||c[i][j]=='O'){
					k=1;
					printf("Casilla ocupada\n");
				}
				break;
			}
			case '8':{
				i=2;
				j=1;
				if(c[i][j]=='X' ||c[i][j]=='O'){
					k=1;
					printf("Casilla ocupada\n");
				}
				break;
			}
			case '9':{
				i=2;
				j=2;
				if(c[i][j]=='X' ||c[i][j]=='O'){
					k=1;
					printf("Casilla ocupada\n");
				}
				break;
			}
		}
	}while(k==1);
	c[i][j]=b;
}
char next(char a){
	if(a=='X'){
		return 'O';
	}else{
		return 'X';
	}
}
int ganador(char c[3][3]){
	int i,j,aux;
	for(i=0;i<3;i++){
		aux=0;
		for(j=0;j<3;j++){
			if(c[i][0]==c[i][j]){
				aux++;
			}
		}
		if(aux==3){
			return 1;
		}
	}
	for(i=0;i<3;i++){
		aux=0;
		for(j=0;j<3;j++){
			if(c[0][i]==c[j][i]){
				aux++;
			}
		}
		if(aux==3){
			return 1;
		}
	}
	if(c[0][0]==c[1][1]&&c[0][0]==c[2][2]){
		return 1;
	}else if(c[0][2]==c[1][1]&&c[0][2]==c[2][0]){
		return 1;
	}else{
		return 0;
	}
}
void loop(char c[3][3],char b){
	llenar(c);
	int i,r;
	r=0;
	for(i=0;i<9&&r!=1;i++){
		tablero(c);
		introducir(c,b);
		system("clear");
		r=ganador(c);
		if(r==1){
			tablero(c);
			printf("Gana jugador %c\n",b);
		}else{
			b=next(b);
		}
	}
	if(r==0){
		tablero(c);
		printf("Empate\n");
	}
}
int main(){
	char c[3][3];
	char jugador='X';
	loop(c,jugador);
	return 0;
}

